// -*- C++ -*-
/* 
 * Author: 
 Fabrice Balli <fabrice.balli@cern.ch>
 Tairan Xu <tairan.xu@cern.ch>
 */

#ifndef MiniTreeMaker13TeV_h
#define MiniTreeMaker13TeV_h

// C++ includes
#include <iostream>
#include <string>
#include <map>
#include <unordered_map>

// xAOD ROOT access
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODCore/ShallowCopy.h"
#ifndef __MAKECINT__
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/EgammaxAODHelpers.h"
#endif // not __MAKECINT__
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODCaloEvent/CaloClusterAuxContainer.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODCore/tools/IOStats.h"
#include "xAODCore/tools/ReadStats.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODBase/IParticle.h"
#include "xAODBase/IParticleContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODEgamma/Egamma.h"
#include "xAODEgamma/ElectronAuxContainer.h"
#include "xAODBase/IParticleHelpers.h"
#include "CPAnalysisExamples/errorcheck.h"
// ROOT includes
#include "TFile.h"
#include "TString.h"
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "TEnv.h"
#include "TTree.h"
#include "TRandom3.h"
#include <vector>
#include "assert.h"
// Tools
#include "ElectronEfficiencyCorrection/AsgElectronEfficiencyCorrectionTool.h"
#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"
#include "ElectronPhotonSelectorTools/AsgElectronIsEMSelector.h"
//#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
//#include "ElectronPhotonShowerShapeFudgeTool/ElectronPhotonShowerShapeFudgeTool.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"

#include "IsolationCorrections/IIsolationCorrectionTool.h"
#include "IsolationSelection/IsolationSelectionTool.h"
//#include "ElectronIsolationSelection/CaloIsoCorrection.h"
#include "TrackVertexAssociationTool/TightTrackVertexAssociationTool.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "AsgTools/AsgMessaging.h"
#include "AsgTools/AnaToolHandle.h"
#include "MiniTreeMaker13TeV/initfinal.h"
#include "MiniTreeMakerCommon.h"

#ifndef __MAKECINT__
template <class T>
void deleteStlVector(T* container);
#endif // not __MAKECINT__


class MiniTreeMaker13TeV : public asg::AsgMessaging{
public: 
  // Constructor
    MiniTreeMaker13TeV(void); //!
  // Destructor
  ~MiniTreeMaker13TeV(); //!
  
  // Initialise
  void Initialise(xAOD::TEvent& event, TString daod); //!
  // Per event analysis //!
  void ProcessEvent(xAOD::TEvent& event); //!
  // Finalise
  void FinaliseEvent(xAOD::TEvent& m_event); //!
  
private:
  bool m_isMC;
  xAOD::TEvent* m_event; //!
  initfinal *initool; 
  // Use Truth: 0: no 1: yes
  int m_truth = 0; //!
  unsigned int evtNbr; //!
  float m_weight; //!
  int StandardMET(xAOD::TEvent&); //!
  void InitialiseVec(); //!
  void fillMuonsVariables();
  void fillInclMuonsVariables();
  void fillElectronsVariables();
  void fillRecoilVariables(bool isel, bool ismu, xAOD::TEvent& event);
  void fillCaloCluster(int i);

    void SelectRecoilLeptons(xAOD::IParticleContainer* init, xAOD::IParticleContainer* initadd, bool init_isel);
    bool  OKnom;
    TLorentzVector zboson, lepton1, lepton2; //!
    float avmu;
    float actmu;
    bool el_iso;
    bool mu_iso;
    unsigned int ngoodmu, ngoodel;
    const xAOD::Vertex *pv;
    Anahelp *an;
    TFile *fout;
    std::string settings_file = "";

  public:
    void set_config(std::string d){settings_file = d;};
    void AddCutflow(TFile *f, xAOD::TEvent& event){an->Anahelp::AddCutflow(f, event, initool->m_daod);};
    void Finalise(){initool->Finalise();};
};

#endif
