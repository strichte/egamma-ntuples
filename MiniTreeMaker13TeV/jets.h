
#ifndef JETS_H
#define JETS_H

// xAOD ROOT access
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODBase/IParticle.h"
#include "xAODBase/IParticleContainer.h"
#include "xAODBase/IParticleHelpers.h"
#ifndef __MAKECINT__
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#endif // not __MAKECINT__

//tools
#include "JetCalibTools/JetCalibrationTool.h"
#include "JetSelectorTools/JetCleaningTool.h"
#include "JetResolution/JERTool.h"
#include "JetResolution/JERSmearingTool.h"
#include "JetUncertainties/JetUncertaintiesTool.h"

#include "JetJvtEfficiency/JetJvtEfficiency.h"
#include "AssociationUtils/OverlapRemovalInit.h"
#include "AssociationUtils/OverlapRemovalTool.h"
#include "AssociationUtils/MuJetOverlapTool.h"
#include "AssociationUtils/EleJetOverlapTool.h"

#include "AsgTools/AnaToolHandle.h"

class JERSmearingTool;
class JetUncertaintiesTool;

class jets {
    friend class initfinal;
public:
  jets();
  virtual ~jets();

  //virtual void finalise();

  void InitialiseJetTools(bool isMC);

int StandardJetSelection(xAOD::TEvent& m_event, xAOD::JetContainer* jetcont);

    TString m_JetAlgo;
    TString m_JetConfig;    
    TString m_JetCalibSeq;      
    TString m_JetCleaningCutLevel;  
    bool m_JetCleaningDoUgly;   
    TString m_JERPlotFileName;  
    TString m_JERCollectionName;  
    bool m_JetSmearingApplyNominal; 
    TString m_JetSmearingSystematicMode; 
    TString m_JesUncerJetDefinition;  
    TString m_JesUncerMCType;   
    TString m_JesUncerConfigFile; 

private:
    bool m_isMC;

    JetCalibrationTool* m_jetCalibTool; //!
    //asg::AnaToolHandle<IJetCalibrationTool> m_jetCalibTool;
    std::map<std::string,JetCleaningTool*>  m_jetCleaningTools; //!
    JetCleaningTool*  m_jetCleaningTool; //!

    CP::JetJvtEfficiency * m_jetJVTTool = 0;
    JERSmearingTool *m_jetSmearingTool;//!    
    //JetUncertaintiesTool *m_jesUncertainty;//!
    JERTool *m_JERTool; //! 
    ORUtils::ORFlags  orFlags;
    ORUtils::ToolBox  toolBox;


};
#endif



