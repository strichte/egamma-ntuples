
#ifndef INITFINAL_H
#define INITFINAL_H
/*
//
// This class calls all tools of the main code class to initialise them, except for GRL and PRW Base tools that are directly initilised here
//
//
//
*/

#include <iostream>
#include <string>
#include <map>
#include <unordered_map>

#include "xAODEventInfo/EventInfo.h"
#include "xAODCore/tools/IOStats.h"
#include "xAODCore/tools/ReadStats.h"
// xAOD ROOT access
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

// xAOD ROOT access
#include "MiniTreeMaker13TeV/AnaHelper.h"
#ifndef __MAKECINT__
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#endif // not __MAKECINT__

//systematics
#include "PATInterfaces/SystematicVariation.h"
#include "PATInterfaces/SystematicsUtil.h"
#include "PATInterfaces/SystematicRegistry.h"
#include "PATInterfaces/MakeSystematicsVector.h"
//tools
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "PileupReweighting/PileupReweightingTool.h"
#include "CPAnalysisExamples/errorcheck.h"
#include "AsgTools/AsgMessaging.h"

#include "MiniTreeMaker13TeV/elecs.h"
#include "MiniTreeMaker13TeV/muons.h"
#include "MiniTreeMaker13TeV/jets.h"
#include "MiniTreeMaker13TeV/trigger.h"
#include "MiniTreeMaker13TeV/MCtruth.h"
#include "MiniTreeMaker13TeV/AnaHelper.h"



namespace CP{
  class PileupReweightingTool;
  class MakeSystematicsVector;
}

class initfinal : public asg::AsgMessaging {
  friend class MiniTreeMaker13TeV;
public:
  initfinal(void);
  virtual ~initfinal();
  virtual void Finalise();
  void Initialise(xAOD::TEvent& event, std::string f_init, TString daod_type);
  //Tree helper, needs to be accessible by the main code
  
  //auto detect the DAOD type, needs to be accessible from the main program
  TString m_daod;
  bool m_isMC; //!
  MCtruth *mctruth ;
  bool m_DEBUG;
  bool m_do5TeV; //!
  // PU reweighting: 0: off, 1: on 
  int m_pureweighting = 0; //!
  CP::PileupReweightingTool *m_PileupReweighting;//! 
  void FinaliseSyst(); //!

private:
  std::string conf_file;
  std::string m_rootCoreBin;
  Anahelp *ana;
  elecs *eltool;
  muons *mutool;
  jets *jettool;
  trigger *trigtool;
  TString m_muonTrigMatch, m_elecTrigMatch, m_elecMCTrigMatch;
  std::string m_ElToolRecoCorrFile, m_ElToolLLHCorrFile, m_ElToolIsoCorrFile, m_ElToolTrigCorrFile, m_ElToolTrigEffCorrFile, m_ElToolIsoVetoCorrFile;
  unsigned int m_mcChannelNumber; 
  //recoil lepton veto requirements
  bool m_elecRecoilMedium, m_elecRecoilTight, m_muonRecoilTight;
  float m_muonMinRecoilPt, m_elecMinRecoilPt;
  
//variables set during init and reading of config file
    bool m_STDM, m_truth; //!
    bool isSherpa, isPowheg, m_doTruth, m_doAcc, m_antiIso, m_forceTruth, m_PassThrough;
    bool m_dosys, m_dosysEL, m_dosysMU; //!
    bool m_doJpsi; //!
    TString m_sysname; //!
    std::vector<std::string > sysnames; //!
    
    TString m_LumiCalcFile;   
    TString m_PUConfFile;   
    std::vector<TString> m_vecLumiCalcFile; 
    std::vector<TString> m_vecPUConfFile;   
    int m_PUUnrepresentedDataAction;
    float m_PUDataScaleFactor;  
    int m_PUDefaultChannel; 
    
    TString m_GRLlist;    
    std::vector<TString> m_GRLVec;
    bool m_GRLPassThrough;

    TString outName;

    TString m_passTriggerMuonMC, m_passTriggerMuonData, m_passTriggerElecMC, m_passTriggerElecData;

    // Tools
    GoodRunsListSelectionTool* m_grlTool;  //!
    // list of systematics
    std::vector<CP::SystematicSet> m_sysListCB; //!
    std::map<std::string, std::vector<CP::SystematicSet> *> * m_sysListSF_corr ;//!
    std::map<std::string, std::map<std::string, CP::SystematicSet *> *> * m_sysListSF_uncorr; //!
    std::map<std::string, std::string> * sysname_uncorr; //!
    std::map<std::string, std::string>  sysname_muSF_effi; //!
    std::map<std::string, std::string>  sysname_muSF_iso; //!
    std::map<std::string, std::string>  sysname_muSF_ttva; //!
    std::map<std::string, std::string>  sysname_muSF_trig; //!
    // a set of list for different tools!
    std::vector<CP::SystematicSet> _listSF1, _listSF2, _listSF3, _listSF4, _listSF5, _listSF6, _listSF7, _listSF8, _listSF9, _listSF9m, _listSF9t, _listSF10, _listSF11, _listSF12, _listSF13, _listSF14, _listSF15, _listSF16;
	std::map<std::string, CP::SystematicSet *> _listuncorr1,  _listuncorr2,  _listuncorr3,  _listuncorr4, _listuncorr5,  _listuncorr6,  _listuncorr7,  _listuncorr8;
    std::string sysname_elid, sysname_eliso, sysname_eltrig;

    //GRL and PRW init
    void InitialisePRW(); //!
    void InitialiseGRL(); //!
    //systematics initialisation
    void InitSystVariation(); //!
    void InsertSystFULL(AsgElectronEfficiencyCorrectionTool * rcTool, std::vector<CP::SystematicSet> * systList_corr, std::vector<CP::SystematicSet> * systList_uncorr) ;
    void InsertSystFULL(CP::MuonEfficiencyScaleFactors * rcTool, std::vector<CP::SystematicSet> * systList_corr , std::vector<CP::SystematicSet> * systList_uncorr, std::map<std::string, std::string> &maptool);
    void InsertSyst(AsgElectronEfficiencyCorrectionTool * rcTool, std::vector<CP::SystematicSet> * systList );
    void InsertSyst(CP::EgammaCalibrationAndSmearingTool * rcTool, std::vector<CP::SystematicSet> * systList );
    void InsertSyst(CP::MuonCalibrationAndSmearingTool * rcTool, std::vector<CP::SystematicSet> * systList );
    void InsertSyst(CP::MuonEfficiencyScaleFactors * rcTool, std::vector<CP::SystematicSet> * systList );
    void InsertSyst(CP::MuonTriggerScaleFactors * rcTool, std::vector<CP::SystematicSet> * systList );
	void MapSyst(std::vector<CP::SystematicSet > * systList, std::map<std::string, CP::SystematicSet *> * systMap);
    void InitialiseCutflow();
    void InitialiseTriggerTools();//!
    void read(std::string);
    void Print();
    void clear();
    TString Generator(int mcChannelNumber);

  };
#endif
