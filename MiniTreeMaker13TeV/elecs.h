
#ifndef ELECS_H
#define ELECS_H

// xAOD ROOT access
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODBase/IParticle.h"
#include "xAODBase/IParticleContainer.h"
#include "xAODBase/IParticleHelpers.h"
#ifndef __MAKECINT__
#include "xAODEgamma/ElectronContainer.h"
#endif // not __MAKECINT__

#include "AsgTools/AnaToolHandle.h"
#include "CPAnalysisExamples/errorcheck.h"

#include "IsolationCorrections/IIsolationCorrectionTool.h"
#include "IsolationSelection/IsolationSelectionTool.h"

#include "ElectronEfficiencyCorrection/AsgElectronEfficiencyCorrectionTool.h"
#include "ElectronPhotonSelectorTools/AsgForwardElectronIsEMSelector.h"
#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"
#include "ElectronPhotonSelectorTools/AsgElectronIsEMSelector.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "MiniTreeMaker13TeV/trigger.h"

class AsgElectronLikelihoodTool;
class AsgElectronEfficiencyCorrectionTool;
class AsgPhotonIsEMSelector;
class ElectronPhotonShowerShapeFudgeTool;
namespace CP{
  class EgammaCalibrationAndSmearingTool;
  class IsolationSelectionTool;
}

class elecs {
    friend class initfinal;
    friend class MiniTreeMaker13TeV;
public:
  elecs();
  virtual ~elecs();
  //virtual void finalise();

  void InitialiseElecTools(bool isMC);
int StandardElecSelection(xAOD::TEvent& m_event, const xAOD::EventInfo& eventInfo, xAOD::ElectronContainer* elcont, asg::AnaToolHandle<Trig::IMatchingTool> m_elTrigMatch);
int AntiIsoElecSelection(xAOD::TEvent& m_event, const xAOD::EventInfo& eventInfo, xAOD::ElectronContainer* elcont, asg::AnaToolHandle<Trig::IMatchingTool> m_elTrigMatch);
int ForwardElecSelection(xAOD::TEvent& m_event, xAOD::ElectronContainer* elcont);
  std::vector < TString >  m_ElToolRecoCorrFilevec ;
  std::vector < TString >  m_ElToolLLHCorrFilevec_loose ;

  std::vector < TString >  m_ElToolLLHCorrFilevec_medium  ;
  std::vector < TString >  m_ElToolIsoCorrFilevec_medium  ;
  std::vector < TString >  m_ElToolLLHCorrFilevec_tight  ;
  std::vector < TString >  m_ElToolIsoCorrFilevec_tight  ;
  std::vector < TString >  m_ElToolTrigCorrFilevec  ;
  std::vector < TString >  m_ElToolTrigEffCorrFilevec   ;
  std::vector < TString >  m_ElToolIsoVetoCorrFilevec  ;

  TString EL_CorrModel; //correlation model : FULL, etc...
  int m_ElToolForcedataType;
  TString m_ElToolCalibAndSmearESModel;
  TString m_ElToolCalibAndSmearDecorrModel;
  TString m_ElToolLlhPVCont;
  TString m_ElToolLlhLooseConfigFile;
  TString m_ElToolLlhMediumConfigFile;
  TString m_ElToolLlhTightConfigFile;
  TString m_ElFwdIDConfigFile;
  TString m_IsolToolElecWP;

  float m_elecMaxEta        ;
  float m_elecMinPt         ;
  float m_elecMaxFwdEta     ;
  float m_elecMinFwdPt      ;
  float m_elecMinEtaCrack   ;
  float m_elecMaxEtaCrack   ;
  float m_elecFinalMinPt    ;
  float m_elecPtMaxTrig ;
  std::vector<TString> electrigmatchnames; //!
  std::vector<TString> elecmctrigmatchnames; //!

private:
  bool m_isMC, m_antiIso;
  unsigned int ngoodel;
  AsgElectronEfficiencyCorrectionTool * m_elRecoCorr;
  AsgElectronEfficiencyCorrectionTool * m_elLLHCorr_loose ;

  AsgElectronEfficiencyCorrectionTool * m_elLLHCorr_medium ;
  AsgElectronEfficiencyCorrectionTool * m_elIsoCorr_medium ;
  AsgElectronEfficiencyCorrectionTool * m_elLLHCorr_tight ;
  AsgElectronEfficiencyCorrectionTool * m_elIsoCorr_tight ;
  AsgElectronEfficiencyCorrectionTool * m_elTrigCorr ;
  AsgElectronEfficiencyCorrectionTool * m_elTrigEff ;
  AsgElectronEfficiencyCorrectionTool * m_elIsoVetoCorr ;

  std::vector<std::string> inputFile1;
  std::vector<std::string> inputFile2_loose;
  std::vector<std::string> inputFile2_medium;
  std::vector<std::string> inputFile3_medium;
  std::vector<std::string> inputFile2_tight;
  std::vector<std::string> inputFile3_tight;
  std::vector<std::string> inputFile4;
  std::vector<std::string> inputFile5;
  std::vector<std::string> inputFile6;


  CP::EgammaCalibrationAndSmearingTool* m_egammaCalibrationAndSmearingTool ;
  //CP::TightTrackVertexAssociationTool* trkelver;
  AsgForwardElectronIsEMSelector* m_electronFwdIDTool; //!
  AsgElectronLikelihoodTool* m_electronLikelihoodToolLoose ;
  AsgElectronLikelihoodTool* m_electronLikelihoodToolMedium ;
  AsgElectronLikelihoodTool* m_electronLikelihoodToolTight ;

  //for both mu and el
  CP::IsolationSelectionTool* m_isolationSelectionTool;


};
#endif
