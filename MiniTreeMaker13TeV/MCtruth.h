
#ifndef MCTRUTH_H
#define MCTRUTH_H

// xAOD ROOT access
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODBase/IParticle.h"
#include "xAODBase/IParticleContainer.h"
#include "xAODMissingET/MissingETContainer.h"

#ifndef __MAKECINT__
#include "xAODJet/JetContainer.h"
#endif // not __MAKECINT__



class MCtruth {
 public:
  MCtruth(bool old=false, bool Sherpa=false, bool Powheg=true, bool SkipDress=false);
  virtual ~MCtruth();
  int execute(xAOD::TEvent& m_event);
  std::vector<xAOD::TruthParticle*> getPhotons();
  std::vector<xAOD::Jet*>  getJets();
  xAOD::TruthParticle* getNeutrino();
  std::vector<xAOD::TruthParticle*> getBareLeps();
  std::vector<xAOD::TruthParticle*> getBornLeps();
  std::vector<xAOD::TruthParticle*> getDressedLeps();
  xAOD::TruthParticle* getBoson();
  std::vector< std::string > tmet_source;
  std::vector< float > tmet_pt;
  std::vector< float > tmet_phi;
  std::vector< float > tmet_sumet;
  std::pair<int, int> getPDGID();
  virtual void finalise();

 private:
  bool oldFormat, isSherpa, isPowheg, skipDress;
  int pdg1, pdg2;
  xAOD::TruthParticle* trueBoson;
  xAOD::TruthParticle* neutrino;
  std::vector<xAOD::TruthParticle*> bareLeps, bornLeps, dressedLeps, truthPhotons;
  std::vector<xAOD::Jet*>truthJets; 

  float deltaPhi(float phi1, float phi2) {
     float dphi = std::fabs(phi1-phi2);
     if (dphi > TMath::Pi()) dphi = 2*TMath::Pi() - dphi;
    return dphi;
   }

  float deltaR(float eta1, float phi1, float eta2, float phi2) {
   float deta = eta1-eta2;  
   float dphi = deltaPhi(phi1,phi2);   
   return sqrt(deta*deta+dphi*dphi);  
  } 

};
#endif
