#ifndef JETS_CXX
#define JETS_CXX

#include "MiniTreeMaker13TeV/jets.h"

// Constructor
jets::jets() {		
		return;
}
// Destructor
jets::~jets() {
}



int jets::StandardJetSelection(xAOD::TEvent& m_event, xAOD::JetContainer* jetcont)
{
	// Get the jets
	//std::cout << "function : "<< __FUNCTION__ <<", line = " << __LINE__ <<std::endl;
    
	const xAOD::JetContainer* jets = 0;
	if ( !m_event.retrieve( jets, "AntiKt4EMTopoJets" ).isSuccess() ) 
	  {
	  Error("execute()", "Failed to retrieve the jets.Exiting." );
	  return -1;
	  }
	//std::cout << "function : "<< __FUNCTION__ <<", line = " << __LINE__ <<std::endl;
    
	// create a shallow copy of the jet container
	std::pair<xAOD::JetContainer*, xAOD::ShallowAuxContainer*> jets_shallowCopy = xAOD::shallowCopyContainer( *jets );
	
	if(!xAOD::setOriginalObjectLink(*jets, *(jets_shallowCopy.first)))
	  {//tell calib container what old container it matches
	  std::cout << "Failed to set the original object links" << std::endl;
	  exit(10);
	  }
	 //std::cout << "function : "<< __FUNCTION__ <<", line = " << __LINE__ <<std::endl;
    
	xAOD::JetContainer::iterator jetSC_itr = (jets_shallowCopy.first)->begin();
	xAOD::JetContainer::iterator jetSC_end = (jets_shallowCopy.first)->end();
	
	for( ; jetSC_itr != jetSC_end; ++jetSC_itr ) 
	   {
	   if( ! m_jetCleaningTool->accept(**jetSC_itr)) continue;
	   //std::cout << "function : "<< __FUNCTION__ <<", line = " << __LINE__ <<std::endl;
    
	   if (m_jetCalibTool->applyCalibration(**jetSC_itr) == CP::CorrectionCode::Error) 
	     {
	     Error("execute()", "JetCalibrationTool returns Error CorrectionCode");
	     } 
		//std::cout << "function : "<< __FUNCTION__ <<", line = " << __LINE__ <<std::endl;
    
	   bool passJvt = ( (*jetSC_itr)->pt()>60e3 || fabs((*jetSC_itr)->eta())>2.4 ||  m_jetJVTTool->passesJvtCut(**jetSC_itr));
	   if (!passJvt) continue;
	   //std::cout << "function : "<< __FUNCTION__ <<", line = " << __LINE__ <<std::endl;
    
		//jet SF of JVT
		//jetsf->getEfficiencyScaleFactor(*jet,sf);
		

	   if(m_isMC)
	     {
	     //m_jesUncertainty->applyCorrection(**jetSC_itr);
	     m_jetSmearingTool->applyCorrection(**jetSC_itr);
	     }
	     //std::cout << "function : "<< __FUNCTION__ <<", line = " << __LINE__ <<std::endl;
    
	   jetcont->push_back( new xAOD::Jet(**jetSC_itr) );
	   }
	   //std::cout << "function : "<< __FUNCTION__ <<", line = " << __LINE__ <<std::endl;
    
	//delete jets;
	delete jets_shallowCopy.first;
	delete jets_shallowCopy.second;

	//m_event.record( jetcont, "jetcont"  );
	//m_event.record( jetcontAux, "jetcontAux."  );
	if( jetcont->size() <1 )return 1;
	return 0;
}


// Initialise the jet tools (cleaning, calibration)
void jets::InitialiseJetTools(bool isMC) {
	m_isMC = isMC;
	// initialise jet calibration tool
	const std::string name = "CalibTool_AntiKt4EMTopo";
	TString jetAlgo = m_JetAlgo;

	TString config = m_JetConfig;
	TString calibSeq = m_JetCalibSeq;
	bool m_isData = m_isMC ? false : true;
	m_jetCalibTool = new JetCalibrationTool(name);
	m_jetCalibTool->setProperty("JetCollection",jetAlgo.Data());
	m_jetCalibTool->setProperty("ConfigFile",config.Data());
	m_jetCalibTool->setProperty("CalibSequence",calibSeq.Data());
	m_jetCalibTool->setProperty("CalibArea","00-04-81");
	m_jetCalibTool->setProperty("IsData",m_isData);
	m_jetCalibTool->setProperty("OutputLevel", MSG::INFO);

	m_jetCalibTool->initializeTool(name);
	
	// initialise jet cleaning tool
	m_jetCleaningTool = new JetCleaningTool("JetCleaning");
	m_jetCleaningTool->setProperty("OutputLevel", MSG::INFO);
	m_jetCleaningTool->setProperty( "CutLevel", m_JetCleaningCutLevel.Data());
	m_jetCleaningTool->setProperty("DoUgly", m_JetCleaningDoUgly);
	m_jetCleaningTool->initialize();

	// initialize JER
	m_JERTool = new JERTool("JERTool");
	m_JERTool ->setProperty( "OutputLevel", MSG::INFO);
	m_JERTool->setProperty("PlotFileName", m_JERPlotFileName.Data());
	m_JERTool->setProperty("CollectionName", m_JERCollectionName.Data());
	m_JERTool->initialize();

	m_jetJVTTool = new CP::JetJvtEfficiency("jetsf");
//	m_jetJVTTool->setProperty("WorkingPoint","Default");
//	m_jetJVTTool->setProperty("SFFile","JetJvtEfficiency/JvtSFFile.root");	
	m_jetJVTTool->initialize();


	m_jetSmearingTool = new JERSmearingTool("JetSmearingTool");
	ToolHandle<IJERTool> jerHandle(m_JERTool->name());
	m_jetSmearingTool->setProperty("JERTool", jerHandle);
	m_jetSmearingTool->setProperty("ApplyNominalSmearing", m_JetSmearingApplyNominal);
	m_jetSmearingTool->setProperty("isMC", m_isMC);
	m_jetSmearingTool->setProperty("SystematicMode", m_JetSmearingSystematicMode.Data());
	m_jetSmearingTool->setProperty("OutputLevel", MSG::INFO);
	m_jetSmearingTool->initialize();


	orFlags = ORUtils::ORFlags("OverlapRemovalTool", "", "overlaps");
//	toolBox = new ORUtils::ToolBox;
	orFlags.outputPassValue = false;
	orFlags.doElectrons = true;
	orFlags.doMuons = true;
	orFlags.doJets = true;
	orFlags.doTaus = false;
	orFlags.doPhotons = false;
	
	ORUtils::recommendedTools(orFlags, toolBox);
	toolBox.setGlobalProperty("OutputLevel", MSG::ERROR);
	toolBox.initialize();

}



#endif
