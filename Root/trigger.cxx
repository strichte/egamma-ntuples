#ifndef TRIGGER_CXX
#define TRIGGER_CXX

#include "MiniTreeMaker13TeV/trigger.h"

// Constructor
trigger::trigger(){		
		return;
}
// Destructor
trigger::~trigger() {
}

int trigger::getTriggerDecision(){
	///////////////
	//// TDT //////
	///////////////
	//only use single-lepton trigger

	passTrigger_el =0;
	passTrigger_mu =0;
	if(!m_isMC)
	  {
	  for(TString str : m_passTriggerElecDatavec)
	    {
	    const Trig::ChainGroup* m_trig = m_tdt->getChainGroup(str.Data());
	    if(m_trig->isPassed()) passTrigger_el =1;
	    }
	  }
	if(m_isMC)
	  {
	  for(TString str : m_passTriggerElecMCvec)
	    {
	    const Trig::ChainGroup* m_trig = m_tdt->getChainGroup(str.Data());
	    if(m_trig->isPassed()) passTrigger_el =1;
	    }
	  }  
	for(TString str : m_passTriggerMuonDatavec)
	  {
	  const Trig::ChainGroup* m_trig = m_tdt->getChainGroup(str.Data());
	  if(m_trig->isPassed()) passTrigger_mu = 1;
	  }

	return 0;
}

void trigger::InitialiseTriggerTools(bool ismc) {
	m_isMC = ismc;
	m_confTool = new TrigConf::xAODConfigTool( "xAODConfigTool" );
	m_confTool->setProperty("OutputLevel", MSG::INFO);
	m_confTool->initialize();
	ToolHandle< TrigConf::ITrigConfigTool > handle( m_confTool );
	m_tdt = new Trig::TrigDecisionTool( "TrigDecisionTool" );
	m_tdt->setProperty("OutputLevel", MSG::INFO);
	m_tdt->setProperty( "ConfigTool", handle );
	m_tdt->setProperty( "TrigDecisionKey", m_TrigDecisionKey.Data());
	m_tdt->initialize();

	ASG_MAKE_ANA_TOOL(m_elTrigMatch, Trig::MatchingTool).ignore();
	m_elTrigMatch.retrieve().ignore();
	ASG_MAKE_ANA_TOOL(m_muTrigMatch, Trig::MatchingTool).ignore();
	m_muTrigMatch.retrieve().ignore();

}


#endif
