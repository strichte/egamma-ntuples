#/bin/bash

curpath=`readlink -f .`
export ninput=1
cat> ./filepath.txt <<EOF
EOF

for i in $(cat ./input);
do
	cat>> ./filepath.txt <<EOF
$i
EOF


echo  "find all path for $i"
for f in `rucio list-files $i | cut -d " " -f 2 | grep root`
do
        path_i=`rucio list-file-replicas $f | grep IN2P3-CC_LOCALGROUPDISK`
	#echo "path_i = $path_i"
	#echo "f = $path_i"
	path_j=`echo $path_i | awk -F 'srm:' '{print $2}'`
	path_j=`echo $path_j | cut -d " " -f 1 `
	#echo $path_j
        fpath="xroot://ccxrootdatlas.in2p3.fr:1094/pnfs${path_j#**pnfs}"
        echo " find sample $f with path:$fpath <<<<<"
	cat>> ./filepath.txt <<EOF
$fpath
EOF
done
done


