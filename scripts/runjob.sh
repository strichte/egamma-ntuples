#!/bin/sh

echo "Running MinitreeMaker13TeV..."
source /afs/in2p3.fr/home/f/fballi/setupAtlasLocalRootbase.sh
echo "sourcing the setup"
source /afs/in2p3.fr/home/f/fballi/private/workspace/w/rc/lowmu/analysis/build/x86_64-slc6-gcc62-opt/setup.sh
echo "running"
runWZ -i SAMPLEPATH -c CONFIG
