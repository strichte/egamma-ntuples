/*
 * Author: Fabrice Balli <fabrice.balli@cern.ch>
 * Author: Tairan Xu <tairan.xu@cern.ch>
 */

#include "MiniTreeMaker13TeV/MiniTreeMaker13TeV.h"
std::vector<TString> vectorise(TString str, TString sep=" ") ;
int main(int argc, char** argv) {

  if (argc < 2 ) {
    std::cerr << "Usage: dxaod2tree --input/-i INPUT_FILE1,INPUT_FILE2,...,INPUT_FILEN --config/-c CONFIG_FILE --nmax/-n NEVENTSMAX" << std::endl;
    return 1;
  }
  else{

    std::vector<TString> infiles;
    std::string configfile;
    bool nocflow = false;
    unsigned int firstEvt = 0;
    unsigned int nmax = 999999999;
    for (int i = 1; i < argc; ++i) {
      if (std::string(argv[i]) == "--input" || std::string(argv[i]) == "-i"){
        if (i + 1 < argc)
         infiles = vectorise(TString(argv[i+1]),",");
       else{
         std::cerr<< "no argument to input file -i" <<std::endl;
         exit(10); 
       }
     }
     else if(std::string(argv[i]) == "--config" || std::string(argv[i]) == "-c"){
      if (i + 1 < argc)
       configfile = std::string(argv[i+1]);
      else{
       std::cerr<< "no argument to config file -c" <<std::endl;
       exit(10); 
      }
     }
     else if(std::string(argv[i]) == "--firstEvent" || std::string(argv[i]) == "-f"){
      if (i + 1 < argc)
       firstEvt = atoi(argv[i+1]);
      else{
       std::cerr<< "no argument to firstEvent -f" <<std::endl;
       exit(10); 
      }
    }
   else if(std::string(argv[i]) == "--noCutflow" || std::string(argv[i]) == "-ncf")
       nocflow = true;
   else if(std::string(argv[i]) == "--nmax" || std::string(argv[i]) == "-n"){
    if (i + 1 < argc)
     nmax = atoi(argv[i+1]);
   else{
     std::cerr<< "no argument to config file -n" <<std::endl;
     exit(10); 
   }
 }

}
xAOD::Init();
  //xAOD::TEvent event;  
  xAOD::TStore store; // create a transient object store (needed for the tools)

  xAOD::TEvent::EAuxMode mode = xAOD::TEvent::kClassAccess; // xAOD::TEvent::kBranchAccess;
  xAOD::TEvent event(mode);
  //std::vector<TString> infiles = vectorise(TString(argv[1]),",");
  
  if (infiles.size()<1) {
    std::cout << "Couldn't find any input files!" << std::endl;
    abort();
  }
  
  for ( TString str : infiles )
    std::cout << str << std::endl;

  TFile *f0 = TFile::Open(infiles[0].Data(),"read");
  if ( !f0->IsOpen() ) {
    std::cout << "Failed to open file " << infiles[0] << std::endl;
    abort();
  }
  
  std::cout << "Reading event from file" << std::endl;
  event.readFrom(f0);
  std::cout << "...Event read ..." << std::endl;
  
    //Attention, Ugly hack
  TString derivationType = "";
  if(infiles[0] .Contains( "DAOD" ) ){
    derivationType = infiles[0].TString::operator()(infiles[0].Index("DAOD") + 5, 5);
    std::cout<<"derivationType = "<<derivationType<<std::endl;
  }
  MiniTreeMaker13TeV analysis;
  
  analysis.set_config(configfile);
  
  std::cout << "Intialising" << std::endl;
  analysis.Initialise(event, derivationType);
  std::cout << "...Initialised !" << std::endl;

  //just to get nevents
  unsigned int ntot =0, itot=0;
  for (const TString& filename : infiles) {
    std::cout << "......Counting number of input events......" << std::endl;
    std::cout << "Opening " << filename << std::endl;
    TFile* f = TFile::Open(filename.Data());
    event.readFrom(f);
    ntot += event.getEntries();
  }

  //if (ntot < nmax || nmax==-9999) nmax = ntot;
  if (ntot < nmax) nmax = ntot;
//  std::cout << nmax <<" Entries will be processed "  << std::endl;
//  TFile *ftest = new TFile("test.root","recreate");
//  event.xAOD::TEvent::writeTo(ftest);
  unsigned int iproc = 0;
  for (const TString& filename : infiles) {
    std::cout << "Opening " << filename << std::endl;
    
    TFile* f = TFile::Open(filename.Data());

    event.readFrom(f);
    
    const unsigned int n_entries = event.getEntries();
    
    std::cout << " Processing file: " << filename << std::endl;
    std::cout << " Entries in file: " << n_entries << std::endl;    
    std::cout << " Now adding cutflow histo" << std::endl;
    if(!nocflow)
      analysis.AddCutflow(f, event);
    std::cout << "cutflow added" << std::endl;
    
    for (unsigned int ientry = 0; (ientry < n_entries) && (itot < nmax); ++ientry) {
      itot++;
     if (iproc < 10 || iproc % 1000 == 0)
       std::cout << " Processing event " << iproc << " / " << nmax << std::endl;
     iproc++;
     if(ientry < firstEvt)
       continue;
     event.getEntry(ientry);
     analysis.ProcessEvent(event);
//     event.getEntry(ientry);
//     event.fill();
//     store.clear();
      //analysis.FinaliseEvent();
   }
 }

       std::cout<< "finalising everything " <<std::endl; 
 analysis.Finalise();
 std::cout << " finalised everything " <<std::endl;
// ftest->Close();
// delete ftest;
 return 0;
}
}
std::vector<TString> vectorise(TString str, TString sep) {
  std::vector<TString> result; TObjArray *strings = str.Tokenize(sep.Data());
  if (strings->GetEntries()==0) { delete strings; return result; }
  TIter istr(strings);
  while (TObjString* os=(TObjString*)istr()) result.push_back(os->GetString());
  delete strings; return result;
}
