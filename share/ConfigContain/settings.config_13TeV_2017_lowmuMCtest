#####
##
#	config file for MiniTreeMaker13TeV analysis code
#	author :
#	fabrice.balli@cern.ch
##
#####
DEBUG:	true
## Outfile name
fileOutName: outTree.root

## 1: Dump these settings, 0: do not
DumpConfigSettings:	1

## do electron or muon systematics (warning : do not do both at the same time)
#NB : stat component for trigger uncertainty is not there yet
DoSystematicElecs:	false
DoSystematicMuons:	false


#name of a particular systematic you want to run over : is not available yet
Systematic:	""

#do anti-isolated selection for MJ background : not available yet
DoAntiIso: false

#setting this flag to true will result in setting all pT_lep cuts to 2 GeV (except for the recoil leptons)
DoJpsi:  false

#enable pile-up reweighting
PileUpReweighting: 0

#true : fill only truth variables, make no cut. Use only with MC
DoAcceptance:	false

#Fill truth variables. Use only with STDM4 MC
DoTruth:	false

#Fill truth variables even if the MC is not STDM4 (e.g. xAOD). Will not fill : truth photons, truth jets, dressed leptons
ForceTruth:	false

#true : force to pass all cuts (= write all events in output tree)
PassThrough:	true


#recoil leptons ID requirements
RecoilElecAtLeastMedium: true
RecoilElecAtLeastTight: false
RecoilMuonAtLeastTight: false


####################
## Muon block
####################

MuonMaxEta:	2.4

#muon quality : (0, 1, 2, 3) = Tight, Medium, Loose, VeryLoose
#not used for the moment (always use medium muons)
MuonQuality:	0

#pT cut
MuonMinPt:	18000.

#pT cut of muons entering the recoil algorithm
MuonMinRecoilPt: 20000.

#pT cut on incl. muon collection (used later on for MET reconstruction)
MuonMinPtIncl:	5000.

#Not used for the moment
MuonFinalMinPt:	24000.

#data and MC muon trigger chains
MuonTrigMatch:	HLT_mu20_iloose_L1MU15,HLT_mu50
MuonMCTrigMatch:	HLT_mu20_iloose_L1MU15_OR_HLT_mu50


####################
## Electron block
####################

ElecMaxEta:	2.47

#pT cut
ElecMinPt:	20000.

#pT cut of electrons entering the recoil algorithm
ElecMinRecoilPt:	20000.

#Remove electrons in the crack
ElecMinEtaCrack:	1.37
ElecMaxEtaCrack:	1.52

#Not used for the moment
ElecFinalMinPt:	24000.

#data and MC electron trigger chains
ElecTrigMatch:	HLT_e60_lhmedium,HLT_e24_lhmedium_L1EM20VH,HLT_e120_lhloose
ElecMCTrigMatch:	HLT_e60_lhmedium,HLT_e24_lhmedium_L1EM18VH,HLT_e120_lhloose


#max pT for which trigger SFs are derived
ElecPtMaxTrig:	150000.


#######################
## PU and GRL 
#######################

LumiCalcFile:	/../../source/MiniTreeMaker13TeV/share/ilumicalc_histograms_None_341294-341649_OflLumi-13TeV-001.root
PUConfFile:	/../../source/MiniTreeMaker13TeV/share/13tevPRW.root

#1 = remove unrepresented data, 2 = leave it there, 3 = reassign it to nearest represented bin
#We always set it to 2, is it better to use 3 (the default value) ?
PUUnrepresentedDataAction:	2
PUDataScaleFactor: 	1
PUDefaultChannel:	361106

GRLlist:	/../../source/MiniTreeMaker13TeV/share/data17_13TeV.periodN_DetStatus-v98-pro21-16_Unknown_PHYS_StandardGRL_All_Good_25ns_ignore_GLOBAL_LOWMU.xml

#true : does not require to pass GRL cut (data)
GRLPassThrough:	true



####################
## Jet  block
####################

## Jet tools initialisation
JetAlgo:			AntiKt4EMTopo
JetConfig:  			JES_MC16Recommendation_Nov2017.config
JetCalibSeq:  			JetArea_Residual_Origin_EtaJES_GSC

JetCleaningCutLevel: 		Loose
JetCleaningDoUgly: 		false

JERPlotFileName: 		JetResolution/Prerec2015_xCalib_2012JER_ReducedTo9NP_Plots_v2.root
JERCollectionName:		AntiKt4EMTopoJets

JetSmearingApplyNominal: 	false
JetSmearingSystematicMode: 	Full

JesUncerJetDefinition:		AntiKt4EMTopo
JesUncerMCType:			MC15
JesUncerConfigFile:		JES_2015/Moriond2016/JES2015_AllNuisanceParameters.config


####################
## Muon tools
####################


MuToolTTVAWP:		TTVA
MuToolTTVACalib:	170916_Rel21PreRec
MuToolEffiWP:		Medium
MuToolEffiCalib:	170916_Rel21PreRec
MuToolIsoWP:		FixedCutLooseIso
MuToolCalibAndSmearStatComb:  false
MuToolTrigEffMuQual:	Medium
MuToolCalibAndSmearYear:	Data16
MuToolCalibAndSmearAlgo:	muons
MuToolCalibAndSmearType:	q_pT
MuToolCalibAndSmearRel:	Recs2016_15_07
MuToolCalibAndSmearSagittaCorr: false
MuToolCalibAndSmearSagittaRel: sagittaBiasDataAll_25_07_17
MuToolCalibAndSmearSagittaMCDist: true

####################
## Electron tools
####################


ElToolRecoCorrFile:	ElectronEfficiencyCorrection/2015_2017/rel21.2/Summer2017_Prerec_v1/offline/efficiencySF.offline.RecoTrk.root

# 2 levels of ID for ID SFs and ISO SFs : medium and tight -- Might need to revisit if we use eventually Loose
ElToolLLHCorrFile_medium:	ElectronEfficiencyCorrection/2015_2017/rel21.2/Summer2017_Prerec_v1/offline/efficiencySF.offline.MediumLLH_d0z0_v13.root
ElToolIsoCorrFile_medium:	ElectronEfficiencyCorrection/2015_2017/rel21.2/Summer2017_Prerec_v1/isolation/efficiencySF.Isolation.MediumLLH_d0z0_v13_isolFixedCutLoose.root
ElToolLLHCorrFile_tight:	ElectronEfficiencyCorrection/2015_2017/rel21.2/Summer2017_Prerec_v1/offline/efficiencySF.offline.TightLLH_d0z0_v13.root
ElToolIsoCorrFile_tight:	ElectronEfficiencyCorrection/2015_2017/rel21.2/Summer2017_Prerec_v1/isolation/efficiencySF.Isolation.TightLLH_d0z0_v13_isolFixedCutLoose.root

ElToolTrigCorrFile:	ElectronEfficiencyCorrection/2015_2016/rel20.7/ICHEP_June2016_v3/trigger/efficiencySF.SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e24_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0.TightLLH_d0z0_v11_isolFixedCutLoose.root
ElToolTrigEffCorrFile:	ElectronEfficiencyCorrection/2015_2016/rel20.7/ICHEP_June2016_v3/trigger/efficiency.SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e24_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0.TightLLH_d0z0_v11_isolFixedCutLoose.root
ElToolForcedataType: 	1
ElToolCalibAndSmearESModel:	es2017_R21_PRE
ElToolCalibAndSmearDecorrModel:	FULL_v1
ElToolLlhPVCont:	PrimaryVertices

#There are 3 ID tools : one for Loose, one for Medium, one for Tight ID
ElToolLlhLooseConfigFile:	ElectronPhotonSelectorTools/offline/mc16_20170828/ElectronLikelihoodLooseOfflineConfig2017_Smooth.conf
ElToolLlhMediumConfigFile:	ElectronPhotonSelectorTools/offline/mc16_20170828/ElectronLikelihoodMediumOfflineConfig2017_Smooth.conf
ElToolLlhTightConfigFile:	ElectronPhotonSelectorTools/offline/mc16_20170828/ElectronLikelihoodTightOfflineConfig2017_Smooth.conf



## Isolation tool : used only for lepton definition in recoil algorithm
IsolToolMuonWP:	FixedCutLoose
IsolToolElecWP: FixedCutLoose

####################
## Trigger tools
####################

## trigger decision tool initialisation
TrigDecisionKey:	xTrigDecision

## trigger chains for trigger decision
passTriggerElecData:	HLT_e24_lhmedium_L1EM20VH,HLT_e60_lhmedium,HLT_e120_lhloose
passTriggerElecMC:	HLT_e24_lhmedium_L1EM18VH,HLT_e60_lhmedium,HLT_e120_lhloose
passTriggerMuonData:	HLT_mu20_iloose_L1MU15,HLT_mu50
passTriggerMuonMC:	HLT_mu20_iloose_L1MU15,HLT_mu50



############
#Recoil
#cut on the neutral PFO pT in MeV
NeutralPFOCut:	500.
###### Not really used ; for standard TST MET
## MET initialisation
METJetSelection:	Tight


#you have to end with an additional line as TEnv does not read the last one...
